DIR_SRC = src
DIR_BUILD = build
DIR_DIST = dist
DIR_PUBLIC = public

SRC_CSS := $(DIR_SRC)/image_comparison_slider.scss
DIST_CSS := $(DIR_DIST)/image_comparison_slider.css
PUBLIC_CSS := $(DIR_PUBLIC)/image_comparison_slider.css

SRC_JS := $(DIR_SRC)/image_comparison_slider.js
SRC_EXTERN_JS := $(DIR_BUILD)/umd.extern.js

DIST_MIN_JS := $(DIR_DIST)/image_comparison_slider.min.js
PUBLIC_MIN_JS := $(DIR_PUBLIC)/image_comparison_slider.min.js

NODE_BIN = node_modules/.bin
NODE_SASS := $(NODE_BIN)/node-sass
CLOSURE_COMPILER := $(NODE_BIN)/google-closure-compiler

.PHONY: all js css clean

all: css js

css: $(PUBLIC_CSS)

js: $(PUBLIC_MIN_JS)

$(PUBLIC_CSS): $(DIST_CSS)
	@cp $< $@

$(PUBLIC_MIN_JS): $(DIST_MIN_JS)
	@cp $< $@

$(DIST_CSS): $(SRC_CSS) | $(DIR_DIST)
	@$(NODE_SASS) --output-style compressed -o $| $<

$(DIST_MIN_JS): $(SRC_JS) package.json package-lock.json | $(DIR_DIST)
	@$(CLOSURE_COMPILER) \
	--compilation_level ADVANCED \
	--env BROWSER \
	--language_out ECMASCRIPT5_STRICT \
	--formatting SINGLE_QUOTES \
	--externs $(DIR_BUILD)/umd.extern.js \
	--js_output_file $@ $<

$(DIR_DIST):
	@mkdir -p $@

clean:
	@rm -rf $(DIR_DIST) $(PUBLIC_CSS) $(PUBLIC_MIN_JS)
