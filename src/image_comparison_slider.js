/* global self */
(function (root, factory) {
  // eslint-disable-next-line no-undef
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module
    // eslint-disable-next-line no-undef
    define([], factory)
  } else if (typeof module === 'object' && module.exports) {
    // Node. Does not work with strict CommonJS, but
    // only CommonJS-like environments that support module.exports,
    // like Node.
    module.exports = factory()
  } else {
    if (!root['gidw']) root['gidw'] = {}
    // Browser globals (root is window)
    root['gidw']['ImageComparisonSlider'] = factory()
  }
}(typeof self !== 'undefined' ? self : this, function () {
  'use strict'

  var _activeListenerOptions = {
    capture: false,
    passive: false
  }

  var _passiveListenerOptions = {
    capture: false,
    passive: true
  }

  /**
   * @param {TouchEvent} event
   * @param {number} id
   * @returns {?Touch}
   */
  function getTouch (event, id) {
    var touches, length, i
    touches = event.changedTouches
    length = touches.length
    for (i = 0; i < length; i++) {
      if (touches[i].identifier === id) return touches[i]
    }
    return null
  }

  /**
   * @constructor
   * @param {Object} [options]
   */
  function ImageComparisonSlider (options) {
    this._handleTouchStart = this._onTouschStart.bind(this)
    this._handleMouseDown = this._onMouseDown.bind(this)
    this._handleMove = this._onMove.bind(this)
    this._handleEnd = this._onEnd.bind(this)

    this._elImageWrapper1 = document.createElement('div')
    this._elImageWrapper2 = document.createElement('div')
    this._elImage1 = document.createElement('img')
    this._elImage2 = document.createElement('img')
    this._elHandle = document.createElement('div')

    this._elImageWrapper1.classList.add('gicsw')
    this._elImageWrapper2.classList.add('gicsw')
    this._elImage1.classList.add('gicsi')
    this._elImage2.classList.add('gicsi')
    this._elHandle.classList.add('gicsh')

    this._elImageWrapper1.appendChild(this._elImage1)
    this._elImageWrapper2.appendChild(this._elImage2)

    this._parentElement = null

    this._image1 = ''
    this._image2 = ''

    this._touchId = undefined

    this._width = 0
    this._height = 0
    this._xMin = 0
    this._xMac = 0
    this._yMin = 0
    this._yMax = 0

    this._sliderValue = 0.5

    this._disabled = false

    this['config'](options)

    this._setStartListeners()
    this._attachToParent()
  }

  Object.defineProperty(ImageComparisonSlider.prototype, 'disabled', {
    get: function () {
      return this._disabled
    },
    set: function (value) {
      this._disabled = !!value
      this._processDisabled()
    }
  })

  /**
   * @param {Object} [options]
   */
  ImageComparisonSlider.prototype['config'] = function (options) {
    if (typeof options === 'object' && options) {
      if (typeof options['element'] === 'string') {
        this._parentElement = document.getElementById(options['element'])
      } else if (options['element'] &&
        options['element'].contains &&
        options['element'].appendChild) {
        this._parentElement = options['element']
      }
      if (typeof options['before'] === 'string') {
        this._image1 = options['before']
      }
      if (typeof options['after'] === 'string') {
        this._image2 = options['after']
      }
      if (typeof options['disabled'] === 'boolean') {
        this._disabled = options['disabled']
      }
    }
    this._processConfig()
  }

  ImageComparisonSlider.prototype._processConfig = function () {
    this._processDisabled()
    this._elImage1.src = this._image1
    this._elImage2.src = this._image2
    this._syncUi()
  }

  ImageComparisonSlider.prototype._processDisabled = function () {
    if (this._disabled) {
      this._removeStartListeners()
      this._end()
      this._elImageWrapper1.classList.add('gicsh-dis')
      this._elImageWrapper2.classList.add('gicsh-dis')
      this._elHandle.classList.add('gicsh-dis')
    } else {
      this._elImageWrapper1.classList.remove('gicsh-dis')
      this._elImageWrapper2.classList.remove('gicsh-dis')
      this._elHandle.classList.remove('gicsh-dis')
      this._setStartListeners()
    }
  }

  ImageComparisonSlider.prototype._syncUi = function () {
    var percent
    percent = this._sliderValue * 100
    this._elImageWrapper2.style.width = percent + '%'
    this._elHandle.style.left = percent + '%'
  }

  ImageComparisonSlider.prototype._onTouschStart = function (event) {
    var touch
    if (this._touchId === undefined &&
        event.changedTouches &&
        event.changedTouches.length === 1) {
      touch = event.changedTouches[0]
      this._touchId = touch.identifier
      this._setTouchListeners()
      this._start(event, touch)
    }
  }

  ImageComparisonSlider.prototype._onMouseDown = function (event) {
    this._setGlobalMouseListeners()
    this._start(event)
  }

  ImageComparisonSlider.prototype._onMove = function (event) {
    var touch
    if (typeof this._touchId === 'number') {
      touch = getTouch(event, this._touchId)
      if (touch) this._move(event, touch)
    } else {
      this._move(event)
    }
  }

  ImageComparisonSlider.prototype._onEnd = function (event) {
    var touch
    if (typeof this._touchId === 'number') {
      if (event.changedTouches) {
        touch = getTouch(event, this._touchId)
        if (touch) this._end()
      } else {
        console.warn('No end for touch event')
      }
    } else {
      this._end()
    }
  }

  /**
   * @param {Event} event
   * @param {Touch} [touch]
   */
  ImageComparisonSlider.prototype._start = function (event, touch) {
    var rect, windowScrollX, windowScrollY

    event.stopImmediatePropagation()
    event.preventDefault()

    this._elHandle.classList.add('gicsh-a')

    windowScrollX = window.scrollX
    windowScrollY = window.scrollY
    rect = this._elImageWrapper1.getBoundingClientRect()
    this._width = rect.width
    this._height = rect.height
    this._xMin = rect.left + windowScrollX
    this._xMax = this._xMin + this._width
    this._yMin = rect.top + windowScrollY
    this._yMax = this._yMin + this._height

    this._moveHandle(event, touch)
  }

  /**
   * @param {Event} event
   * @param {Touch} [touch]
   */
  ImageComparisonSlider.prototype._move = function (event, touch) {
    event.stopImmediatePropagation()
    event.preventDefault()
    this._moveHandle(event, touch)
  }

  ImageComparisonSlider.prototype._end = function () {
    this._touchId = undefined
    this._removeTouchListeners()
    this._removeGlobalMouseListeners()
    this._elHandle.classList.remove('gicsh-a')
  }

  /**
   * @param {Event} event
   * @param {Touch} [touch]
   */
  ImageComparisonSlider.prototype._moveHandle = function (event, touch) {
    var point
    point = touch ? touch.pageX : event.pageX
    if (point > this._xMax) {
      this._sliderValue = 1
    } else if (point < this._xMin) {
      this._sliderValue = 0
    } else {
      this._sliderValue = (point - this._xMin) / this._width
    }
    this._syncUi()
  }

  ImageComparisonSlider.prototype._setTouchListeners = function () {
    this._elHandle.addEventListener(
      'touchend',
      this._handleEnd,
      _passiveListenerOptions
    )
    this._elHandle.addEventListener(
      'touchcancel',
      this._handleEnd,
      _passiveListenerOptions
    )
    this._elHandle.addEventListener(
      'touchmove',
      this._handleMove,
      _activeListenerOptions
    )
  }

  ImageComparisonSlider.prototype._removeTouchListeners = function () {
    this._elHandle.removeEventListener(
      'touchmove',
      this._handleMove,
      _activeListenerOptions
    )
    this._elHandle.removeEventListener(
      'touchcancel',
      this._handleEnd,
      _passiveListenerOptions
    )
    this._elHandle.removeEventListener(
      'touchend',
      this._handleEnd,
      _passiveListenerOptions
    )
  }

  ImageComparisonSlider.prototype._setGlobalMouseListeners = function () {
    window.addEventListener(
      'mouseup',
      this._handleEnd,
      _passiveListenerOptions
    )
    window.addEventListener(
      'mousemove',
      this._handleMove,
      _activeListenerOptions
    )
  }

  ImageComparisonSlider.prototype._removeGlobalMouseListeners = function () {
    window.removeEventListener(
      'mousemove',
      this._handleMove,
      _activeListenerOptions
    )
    window.removeEventListener(
      'mouseup',
      this._handleEnd,
      _passiveListenerOptions
    )
  }

  ImageComparisonSlider.prototype._setStartListeners = function () {
    this._elHandle.addEventListener(
      'touchstart',
      this._handleTouchStart,
      _activeListenerOptions
    )
    this._elHandle.addEventListener(
      'mousedown',
      this._handleMouseDown,
      _activeListenerOptions
    )
  }

  ImageComparisonSlider.prototype._removeStartListeners = function () {
    this._elHandle.removeEventListener(
      'touchstart',
      this._handleTouchStart,
      _activeListenerOptions
    )
    this._elHandle.removeEventListener(
      'mousedown',
      this._handleMouseDown,
      _activeListenerOptions
    )
  }

  ImageComparisonSlider.prototype._attachToParent = function () {
    var fragment
    if (this._parentElement &&
        !this._parentElement.contains(this._elImageWrapper1) &&
        !this._parentElement.contains(this._elImageWrapper2) &&
        !this._parentElement.contains(this._elHandle)) {
      fragment = document.createDocumentFragment()
      fragment.appendChild(this._elImageWrapper1)
      fragment.appendChild(this._elImageWrapper2)
      fragment.appendChild(this._elHandle)
      this._parentElement.appendChild(fragment)
    }
  }

  ImageComparisonSlider.prototype._detachFromParent = function () {
    if (this._parentElement) {
      if (this._parentElement.contains(this._elImageWrapper1)) {
        this._parentElement.removeChild(this._elImageWrapper1)
      }
      if (this._parentElement.contains(this._elImageWrapper2)) {
        this._parentElement.removeChild(this._elImageWrapper2)
      }
      if (this._parentElement.contains(this._elHandle)) {
        this._parentElement.removeChild(this._elHandle)
      }
    }
  }

  ImageComparisonSlider.prototype['destroy'] = function () {
    this._end()
    this._removeStartListeners()
    this._detachFromParent()
    this._parentElement = null
  }

  return ImageComparisonSlider
}))
