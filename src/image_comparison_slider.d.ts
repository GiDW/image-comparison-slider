export = ImageComparisonSlider

declare class ImageComparisonSlider{
  disabled: boolean
  config(options?: ImageComparisonSlider.IImageComparisonSliderOptions): void
  destroy(): void
}

declare namespace ImageComparisonSlider {
  export interface IImageComparisonSliderOptions {
    element?: HTMLElement | string
    before?: string
    after?: string
    disabled?: boolean
  }
}
